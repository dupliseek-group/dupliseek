# DupliSeek

![illustration](https://www.dropbox.com/s/skc9u536r591y19/dupliseek.png?raw=1)

Application to find all your duplicate images.

**To install Dependencies needed:**

Debian/Ubuntu/derivatives:
```
sudo apt install python3-pip
sudo pip3 install pyqt5 opencv-python numpy imutils 
```

Arch:
```
sudo pacman -S python-pip
sudo pip install pyqt5 opencv-python numpy imutils 
```

**to get it:**

```
git clone https://gitlab.com/magnusmj/dupliseek.git
```

**and run main.py**

```
cd dupliseek
python3 main.py
```

SNAP

```
sudo snap install dupliseek
```