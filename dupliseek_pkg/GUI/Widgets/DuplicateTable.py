from PyQt5.QtCore import QSize
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDockWidget, QListWidget, QListWidgetItem

from dupliseek_pkg.GUI import tr, gui_scale


class DupliTableDock(QDockWidget):
	def __init__(self, main_window):
		QDockWidget.__init__(self, main_window)
		self._main_window = main_window
		self.setWindowTitle(tr("Duplicates"))
		self.setObjectName("DupliTableDock")
		self.setMinimumWidth(220*gui_scale())
		self._icon_view = QListWidget(self)
		self._icon_view.setViewMode(QListWidget.IconMode)
		self._icon_view.setIconSize(QSize(200*gui_scale(), 200*gui_scale()))
		self.setWidget(self._icon_view)
		self._icon_view.selectionModel().selectionChanged.connect(self.on_selection_changed)
		self._matches = []

	def on_selection_changed(self, selection):
		for selrange in selection:
			for sel in selrange.indexes():
				duplicates = self._matches[sel.row()]
				self._main_window.on_dupli_table_selection_changed(duplicates)
				return

	def set_duplicates(self, matches):
		self._matches = matches
		self._icon_view.clear()
		for match in matches:
			icon = match[0]
			self._icon_view.addItem(QListWidgetItem(QIcon(icon),""))

	def file_trashed(self, file):
		for match in self._matches:
			if file in match:
				print("removing file from matches: " + file)
				index = match.index(file)
				# item = self._icon_view.itemAt(index, 0)
				del match[index]
				#self._icon_view.takeItem(index)